"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*/

"""


import sys
import numpy as np
from numpy import power as POW
from scipy.special import polygamma as POLYGAMMA
from numpy import sum as SUM
from numpy import log as LOG
from numpy import zeros as ZEROS
from numpy import asarray as ASARRAY
from numpy import mean as MEAN, var as VAR
from numpy import full as FULL
from sklearn.preprocessing import normalize as NORMALIZE
from numpy.linalg import inv as INVERSE
from numpy import matmul as MATMUL
from numpy import exp as EXP
from numpy.random import rand as RAND
from numpy import arange as ARANGE
from numpy import ndarray as ND_ARRAY
from PIL import Image
import pathlib


class helpers:

    @staticmethod
    def geo_transformation(pixels, dimension, pixel_size):
        return ASARRAY([pixels[:, d:d+1]/(1 + SUM(pixels[:, 0:d], axis=1).reshape(pixel_size, 1))
                        for d in range(dimension)]).T.reshape(pixel_size, dimension)

    @staticmethod
    def initial_mix(no_of_clusters):
        return ASARRAY(FULL((1, no_of_clusters), 1/no_of_clusters))

    @staticmethod
    def split_pixels_based_on_label(labels, pixels):
        clusters_obj = {}
        for index, label in enumerate(labels):
            if not (label in clusters_obj):
                clusters_obj[label] = []
            clusters_obj[label].append(pixels[index])
        return clusters_obj, len(clusters_obj[0][0])



    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """

    @staticmethod
    def method_of_moment(no_of_clusters, cluster_set, dimension):
        alpha = []
        beta = []
        for label in cluster_set:
            cluster_set[label] = ASARRAY(cluster_set[label])
            m = MEAN(cluster_set[label], axis=0) + sys.float_info.epsilon
            v = VAR(cluster_set[label], axis=0) + sys.float_info.epsilon
            alpha.append(m * ((m * (m + 1)/v) + 1))
            beta.append((m * (m + 1)/v) + 2)
        alpha = ASARRAY(alpha).reshape(no_of_clusters, dimension)
        beta = ASARRAY(beta).reshape(no_of_clusters, dimension)
        return NORMALIZE(alpha), NORMALIZE(beta)

    # @staticmethod
    # def method_of_moment(no_of_clusters, cluster_set, dimension):
    #     alpha = ZEROS([no_of_clusters, dimension])
    #     for label in cluster_set:
    #         sum_alpha = 0
    #         for d in range(dimension):
    #             cluster_set[label] = ASARRAY(cluster_set[label])
    #             mean = MEAN(cluster_set[label][:, [d]])
    #             den = VAR(cluster_set[label][:, [d]]) + sys.float_info.epsilon
    #             alpha_d_plus_one = ((POW(mean, 2) + mean)/den) + 2
    #             sum_alpha += alpha_d_plus_one
    #             alpha[label][d] = mean * (alpha_d_plus_one - 1)
    #     return NORMALIZE(alpha), NORMALIZE(alpha)

    @staticmethod
    def compute_neighbor(pixels, width, height, canny_edge):
        neighbor = []
        counter = 0
        for j, pixel in enumerate(pixels):
            if counter == width - 1:
                counter = 0
                neighbor.append({'index': None, 'pixel': None})
            else:
                neighbor.append({'index': j, 'pixel': pixels[j + 1]})
                counter += 1
        result = []
        boundary_pixels = []
        img_pixels = []
        n_pixels = ASARRAY(neighbor).reshape(width * height, 1)

        for i, (p_v, n_v, e_v) in enumerate(zip(pixels, n_pixels, canny_edge)):
            n_v = n_v[0]
            if e_v == 255 or n_v['index'] and canny_edge[n_v['index']] == 255:
                boundary_pixels.append({'index': i, 'pixel': p_v})
            else:
                result.append(n_v['pixel'])
                img_pixels.append(p_v)
        return ASARRAY(img_pixels), ASARRAY(result), ASARRAY(boundary_pixels), len(result)


    @staticmethod
    def posterior_estimator(pdf, mix, p_size, k):
        return ASARRAY([(POW(mix, 2) * p_v) / SUM(POW(mix, 2) * p_v) for p_v in pdf]).reshape(p_size, k)
    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def mix_estimator(posterior):
        return ASARRAY(SUM(posterior, axis=0)/len(posterior))


    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def compute_g(img_pixels, n_pixels, alpha, beta, posterior, dim, k):
        q_alpha = []
        q_beta = []
        q_alpha_sqr = []
        q_beta_sqr = []
        q_alpha_beta_square = []

        for index, (a_v, b_v) in enumerate(zip(alpha, beta)):
            a_d_gamma = POLYGAMMA(0, a_v)
            b_d_gamma = POLYGAMMA(0, b_v)
            a_b_d_gamma = POLYGAMMA(0, a_v + b_v)
            a_tri_gamma = POLYGAMMA(1, a_v)
            b_tri_gamma = POLYGAMMA(1, b_v)
            a_b_tri_gamma = POLYGAMMA(1, a_v + b_v)
            a_pixels = [LOG(pixel / (1 + pixel)) for pixel in img_pixels]
            b_pixels = [LOG(1 / (1 + pixel)) for pixel in img_pixels]
            a_n_pixels = []
            b_n_pixels = []
            for n_v in n_pixels:
                if n_v is not None:
                    a_n_pixels.append(LOG(n_v / (1 + n_v)))
                    b_n_pixels.append(LOG(1 / (1 + n_v)))
                else:
                    a_n_pixels.append(ZEROS((1, dim)))
                    b_n_pixels.append(ZEROS((1, dim)))
            a_n_pixels = ASARRAY(a_n_pixels)
            b_n_pixels = ASARRAY(b_n_pixels)

            q_alpha.append(SUM(posterior[:, [index]] * (2 * (a_b_d_gamma - a_d_gamma) + a_pixels), axis=0).reshape(1, dim))
            q_beta.append(SUM(posterior[:, [index]] * (2*(a_b_d_gamma - b_d_gamma) + b_pixels), axis=0).reshape(1, dim))
            q_alpha_sqr.append(SUM(posterior[:, [index]] * 2 * (a_b_tri_gamma - a_tri_gamma), axis=0).reshape(1, dim))
            q_beta_sqr.append(SUM(posterior[:, [index]] * 2 *(a_b_tri_gamma - b_tri_gamma), axis=0).reshape(1, dim))
            q_alpha_beta_square.append(SUM(2 * (posterior[:, [index]] * a_b_tri_gamma), axis=0).reshape(1, dim))
        return ASARRAY(q_alpha).reshape(k, dim), ASARRAY(q_beta).reshape(k, dim), ASARRAY(q_alpha_sqr).reshape(k, dim),\
            ASARRAY(q_beta_sqr).reshape(k, dim), ASARRAY(q_alpha_beta_square).reshape(k, dim)

    @staticmethod
    def fisher_info_inverse(alpha, beta, q_alpha, q_beta, q_alpha_sqr, q_beta_sqr, q_alpha_beta_sqr, k, dim):
        alpha_updated = []
        beta_updated = []

        for a_v, b_v, q_a, q_b, q_a_sqr, q_b_sqr, q_a_b_sqr in zip(alpha, beta, q_alpha, q_beta, q_alpha_sqr,
                                                                   q_beta_sqr, q_alpha_beta_sqr):

            for aVal, bVal, qAVal, qBVal, qASqVal, qBSqVal, qABSqVal in zip(a_v, b_v, q_a, q_b, q_a_sqr,
                                                                            q_b_sqr, q_a_b_sqr):
                hessian_inverse = INVERSE(ASARRAY([qASqVal, qABSqVal, qABSqVal, qBSqVal]).reshape(2, 2))
                theta = ASARRAY([aVal, bVal]).reshape(2, 1) - \
                    0.37 * MATMUL(hessian_inverse, ASARRAY([qAVal, qBVal]).reshape(2, 1))
                alpha_updated.append(theta[0])
                beta_updated.append(theta[1])
        alpha_updated = ASARRAY(alpha_updated).reshape(k, dim)
        beta_updated = ASARRAY(beta_updated).reshape(k, dim)
        print("alpha_updated :>", alpha_updated)
        print("beta_updated :>", beta_updated)
        return alpha_updated, beta_updated


    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def predict_labels(posterior):
        return ASARRAY(posterior.argmax(axis=1))

    def cal_post_boundary_pixels(self, gen_inv_dirichlet, b_pixels_with_index,
                                 posterior,  no_of_clusters, alpha, beta, dim, mix):
        n_pixels = []  # as we do not have any neighbors for boundary pixels.
        b_pixels = [b_v['pixel'] for b_v in b_pixels_with_index]
        pdf_b_pixels = gen_inv_dirichlet(no_of_clusters, alpha, beta, b_pixels, n_pixels).pdf_fetcher()
        posterior_b_pixels = self.posterior_estimator(pdf_b_pixels, mix, len(pdf_b_pixels), no_of_clusters)
        posterior = posterior.tolist()

        for i, p_v in enumerate(posterior_b_pixels):
            posterior.insert(b_pixels_with_index[i]['index'], p_v)
        return ASARRAY(posterior)

    @staticmethod
    def generate_img(k, dim, predict_labels, img_w,  img_h, img_name, img_extension, counter, p_color):
        # p_color = RAND(k, dim) * 0.45
        img_pixels = ND_ARRAY(shape=(img_w * img_h, 5), dtype=float)
        for cluster in range(k):
            for l_index, l_record in enumerate(predict_labels):
                if cluster == l_record:
                    img_pixels[l_index][0] = int(round(p_color[cluster][0] * 255))
                    img_pixels[l_index][1] = int(round(p_color[cluster][1] * 255))
                    img_pixels[l_index][2] = int(round(p_color[cluster][2] * 255))

        # Save image
        image = Image.new("RGB", (img_w, img_h))

        for y in range(img_h):
            for x in range(img_w):
                image.putpixel((x, y), (int(img_pixels[y * img_w + x][0]),
                                        int(img_pixels[y * img_w + x][1]),
                                        int(img_pixels[y * img_w + x][2])))

        pathlib.Path('./output/output_' + img_name).mkdir(parents=True, exist_ok=True)
        print("./output/output_" + img_name + "/" + str(counter) + img_extension)
        image.save("./output/output_" + img_name + "/" + str(counter) + img_extension)

    @staticmethod
    def save_labels(img_name, data, counter):
        np.save('./output/output_' + img_name + '/' + 'segment_obj_' + str(counter) + img_name, data)
