import sys
from dataSet import DataSet  # Importing DataSet
from KMeans import KMeans as KM # contains KNN related functionality.
from lib.helpers import helpers as HELPER  # class contains the logic like performanceMeasure, Precision etc.
from lib.constants import CONST  # contains the constant values.
from gen_inverted_dirichlet import GeneralizedInvertedDirichlet as GID
from canny_edge_detector import Canny

from sklearn.preprocessing import normalize as NORMALIZE
from numpy import asarray as ASARRAY
import warnings
warnings.filterwarnings('error')




def initial_algorithm(no_of_clusters, img_name):
    pixels, img_width, img_height = DataSet(img_name).pixel_extractor()
    pixels = NORMALIZE(pixels)
    pixels += sys.float_info.epsilon
    p_size = len(pixels)
    p_labels = ASARRAY(KM(pixels, no_of_clusters).predict()).reshape(1, p_size)
    canny_edge = Canny().edge_detector(img_name).reshape(img_height * img_width, 1)
    cluster_set, dim = HELPER().split_pixels_based_on_label(p_labels[0], pixels)
    pixels = HELPER().geo_transformation(pixels, dim, p_size)
    mix_initial_alg = HELPER().initial_mix(no_of_clusters)
    alpha_initial, beta_initial = HELPER().method_of_moment(no_of_clusters, cluster_set, dim)
    updated_pixels, n_pixels, b_pixels, p_size = HELPER().compute_neighbor(pixels, img_width, img_height, canny_edge)
    return alpha_initial, beta_initial, updated_pixels, n_pixels, b_pixels, dim, p_size, mix_initial_alg, img_height, img_width


def estimation_step(k_param, mix_param, alpha_param, beta_param,  pixels_param, n_pixels, p_size):
    pdf_e_step = GID(k_param, alpha_param, beta_param, pixels_param, n_pixels).pdf_fetcher()
    posterior_e_step = HELPER().posterior_estimator(pdf_e_step, mix_param, p_size, k_param)
    return posterior_e_step

def maximization_step(k_param, alpha_param, beta_param, pixels_param, n_pixels, dim_param, posterior_param):
    mix_m_step = HELPER().mix_estimator(posterior)
    q_alpha, q_beta, q_alpha_sqr, q_beta_sqr, q_alpha_beta_sqr = HELPER().compute_g(pixels_param, n_pixels, alpha_param,
                                                                                    beta_param, posterior_param,
                                                                                    dim_param, k_param)
    alpha_m_step, beta_m_step = HELPER().fisher_info_inverse(alpha, beta, q_alpha, q_beta, q_alpha_sqr,
                                                             q_beta_sqr, q_alpha_beta_sqr, k_param, dim_param)
    return mix_m_step, alpha_m_step, beta_m_step


if __name__ == '__main__':
    k = CONST["K"]

    if len(sys.argv) == 2:
        image = sys.argv[1]
    else:
        image = CONST['IMG']
    image_arr = image.split(".")

    if len(image_arr) == 2:
        image_name = image_arr[0]
        image_extension = '.' + image_arr[1]
    else:
        image_name = image_arr[0]
        image_extension = '.jpg'

    alpha, beta, image_pixels, neighbor_pixels, boundary_pixels, dimension, pixel_size, mix, image_height, image_width = initial_algorithm(k, image)
    counter = 1

    while True:
        posterior = estimation_step(k, mix, alpha, beta, image_pixels, neighbor_pixels, pixel_size)
        mix, alpha, beta = maximization_step(k, alpha, beta, image_pixels, neighbor_pixels, dimension, posterior)
        posterior = HELPER().cal_post_boundary_pixels(GID, boundary_pixels,
                                                      posterior, k, alpha, beta, dimension, mix)
        predict_labels = HELPER().predict_labels(posterior)
        print("Counter :>", counter)
        HELPER().generate_img(k, dimension, predict_labels, image_width, image_height,
                             image_name, image_extension, counter, CONST["PIXEL_COLOR_ONE"])
        HELPER().save_labels(image_name, predict_labels, counter)
        counter = counter + 1
        if counter == 10:
            exit()
