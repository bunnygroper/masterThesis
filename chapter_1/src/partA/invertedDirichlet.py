"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*/

"""

from numpy import sum as SUM
from numpy import subtract as SUBS
from numpy import zeros as ZEROS
from scipy.special import gammaln as GAMMALN
from numpy import exp as EXP
from numpy import log as LOG
from numpy import asarray as ASARRAY
import warnings
warnings.filterwarnings("error")

"""
/**
 * This function add the array's element and return them in the form of a String.
 * @param  {Integer} a.
 * @return {String} which contains the Sum of Array.
 */
"""


class InvertedDirichlet:

    def __init__(self, no_of_clusters, alpha, img_pixels, n_pixels, dim):
        self.no_of_clusters = no_of_clusters
        self.alpha = alpha
        self.img_pixels = img_pixels
        self.n_pixels = n_pixels
        self.dim = dim
    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    def pdf_fetcher(self):
        probability = ZEROS((len(self.img_pixels), self.no_of_clusters))
        for a_index, a_v in enumerate(self.alpha):
            if len(self.n_pixels) == 0:
                for p_index, p_v in enumerate(self.img_pixels):
                    n_v = None  # In Order to skip pdf equation containing Neighbor-Pixels.
                    probability[p_index][a_index] = self.pdf(p_v, n_v, a_v, self.dim)
            else:
                for p_index, (p_v, n_v) in enumerate(zip(self.img_pixels, self.n_pixels)):
                    probability[p_index][a_index] = self.pdf(p_v, n_v, a_v, self.dim)
        return probability

    @staticmethod
    def pdf(p_v, n_v, a_v, dim):
        try:
            pixels = EXP(GAMMALN(SUM(a_v)) - SUM(GAMMALN(a_v)) +
                         SUM(SUBS(a_v[:-1], 1) * LOG(p_v)) -
                         SUM(a_v) * LOG(1 + SUM(p_v)))
            if n_v is not None:
                n_v = ASARRAY(n_v).reshape(1, dim)
                n_pixels = EXP(GAMMALN(SUM(a_v)) - SUM(GAMMALN(a_v)) +
                               SUM(SUBS(a_v[:-1], 1) * LOG(n_v)) -
                               SUM(a_v) * LOG(1 + SUM(n_v)))
            else:
                n_pixels = 1
            return pixels * n_pixels
        except RuntimeWarning:
            print("pVector :>", p_v)
            print("aVector :>", a_v)
            print("GAMMALN(SUM(a_v)) :>", GAMMALN(SUM(a_v)))
            print("GAMMALN(a_v) :>", GAMMALN(a_v))
            print("LOG(p_v) :>", LOG(p_v))
            print("SUM(a_v) :>", SUM(a_v))
            print("LOG(1 + SUM(p_v) :>", LOG(1 + SUM(p_v)))
            exit(0)














    # @staticmethod
    # def pdf(p_v, n_v, a_v, dim):
    #     try:
    #         pixels = EXP(GAMMALN(SUM(a_v)) - SUM(GAMMALN(a_v)) +
    #                      SUM(SUBS(a_v[:-1], 1) * LOG(p_v)) -
    #                      SUM(a_v) * LOG(1 + SUM(p_v)))
    #         if n_v is not None:
    #             n_v = ASARRAY(n_v).reshape(1, dim)
    #             n_pixels = EXP(GAMMALN(SUM(a_v)) - SUM(GAMMALN(a_v)) +
    #                            SUM(SUBS(a_v[:-1], 1) * LOG(n_v)) -
    #                            SUM(a_v) * LOG(1 + SUM(n_v)))
    #         else:
    #             n_pixels = 1
    #         return pixels * n_pixels
    #     except RuntimeWarning:
    #         print("pVector :>", p_v)
    #         print("aVector :>", a_v)
    #         print("GAMMALN(SUM(a_v)) :>", GAMMALN(SUM(a_v)))
    #         print("GAMMALN(a_v) :>", GAMMALN(a_v))
    #         print("LOG(p_v) :>", LOG(p_v))
    #         print("SUM(a_v) :>", SUM(a_v))
    #         print("LOG(1 + SUM(p_v) :>", LOG(1 + SUM(p_v)))
    #         exit(0)
