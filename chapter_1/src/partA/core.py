import sys
from dataSet import DataSet  # Importing DataSet
from KMeans import KMeans as KM # contains KNN related functionality.
from sklearn.preprocessing import normalize as NORMALIZE
from numpy import asarray as ASARRAY

from lib.helpers import Helpers as HELPER  # class contains the logic like performanceMeasure, Precision etc.
from lib.constants import CONST  # contains the constant values.
from invertedDirichlet import InvertedDirichlet
from canny_edge_detector import Canny


def initial_algorithm(no_of_clusters, img_name):
    pixels, img_width, img_height = DataSet(img_name).pixel_extractor()
    pixels = NORMALIZE(pixels)
    pixels += sys.float_info.epsilon
    p_labels = ASARRAY(KM(pixels, no_of_clusters).predict()).reshape(1, len(pixels))
    canny_edge = Canny().edge_detector(img_name).reshape(img_height * img_width, 1)
    cluster_set, dim = HELPER().split_pixels_based_on_label(p_labels[0], pixels)
    mix_initial = HELPER().initial_mix(no_of_clusters)
    alpha_initial_alg = HELPER().method_of_moment(K, cluster_set, dim)
    updated_pixels, n_pixels, b_pixels, p_size = HELPER().compute_neighbor(pixels, img_width, img_height, canny_edge)
    return alpha_initial_alg, updated_pixels, n_pixels, b_pixels, dim, p_size, mix_initial, img_height, img_width


def estimation_step(k, mix_param, alpha_param, pixels, n_pixels, p_size, dim):
    pdf_e_step = InvertedDirichlet(k, alpha_param, pixels, n_pixels, dim).pdf_fetcher()
    posterior_e_step = HELPER().posterior_estimator(pdf_e_step, mix_param, p_size, k)
    return posterior_e_step


def maximization_step(no_of_clusters, alpha_param, pixels, n_pixels, dim, posterior_param, p_size):
    mix_m_step = HELPER().mix_estimator(posterior_param)  # Checked: Working Fine!
    gradient = HELPER().gradient_estimation(pixels, n_pixels, p_size, alpha_param, posterior_param, dim)
    h_diagonal, h_constant, hessian_a, hessian_a_t = HELPER().hessian(dim, posterior_param, alpha_param)
    hessian_inverse = HELPER().hessian_inverse(no_of_clusters, h_diagonal, h_constant, hessian_a, hessian_a_t)
    alpha_m_step = HELPER().alpha_updater(alpha_param, hessian_inverse, gradient, no_of_clusters, dim)
    return mix_m_step, alpha_m_step


if __name__ == '__main__':
    K = CONST["K"]

    if len(sys.argv) == 2:
        image = sys.argv[1]
    else:
        image = CONST['IMG']
    image_arr = image.split(".")

    if len(image_arr) == 2:
        image_name = image_arr[0]
        image_extension = '.' + image_arr[1]
    else:
        image_name = image_arr[0]
        image_extension = '.jpg'

    alpha, image_pixels, neighbor_pixels, boundary_pixels, dimension, pixel_size, mix, \
        image_height, image_width = initial_algorithm(K, image)
    counter = 1

    while True:
        posterior = estimation_step(K, mix, alpha, image_pixels, neighbor_pixels, pixel_size, dimension)
        mix, alpha = maximization_step(K, alpha, image_pixels, neighbor_pixels, dimension, posterior, pixel_size)
        posterior = HELPER().cal_post_boundary_pixels(InvertedDirichlet, boundary_pixels,
                                                      posterior, K, alpha, dimension, mix)
        predict_labels = HELPER().predict_labels(posterior)
        HELPER().generate_img(K, predict_labels, image_width, image_height, image_name,
                              image_extension, counter, CONST["PIXEL_COLOR_ONE"])
        HELPER().save_labels(image_name, predict_labels, counter)
        print("alpha : ", alpha)
        counter = counter + 1
        if counter == 20:
            exit()
