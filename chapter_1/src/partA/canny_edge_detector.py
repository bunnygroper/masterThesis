"""
*
*    # Canny Edge Detector Implementation.
*
"""

import numpy as np
import cv2
from numpy import asarray as ASARRAY


class Canny:
    def __init__(self):
        print("Canny Algorithm Under-process!")

    def edge_detector(self, img_name):
        image = cv2.imread('./dataset/images/' + img_name)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (3, 3), 0)
        auto = self.auto_canny(blurred)
        #self.plot_img(auto, image)
        return ASARRAY(auto)

    @staticmethod
    def plot_img(edges, image):
        cv2.imshow("Original", image)
        cv2.imshow("Edges", np.hstack([edges]))
        cv2.waitKey(0)

    @staticmethod
    def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)

        # return the edged image
        return edged
