import sys
import numpy as np
import pickle
from numpy import power as POW
from scipy.special import gamma as GAMMA
from scipy.special import polygamma as POLYGAMMA
from numpy import ndarray as ND_ARRAY
from numpy import sum as SUM
from numpy import multiply as MUL
from numpy import log as LOG
from numpy import zeros as ZEROS
from numpy import transpose as TRANSPOSE
from numpy import asarray as ASARRAY
from numpy import mean as MEAN, var as VAR
from numpy import full as FULL
from numpy import concatenate as CONCAT
from sklearn.preprocessing import normalize as NORMALIZE
from numpy.linalg import inv as INVERSE
from numpy import diag as DIAGONAL
from numpy import matmul as MATMUL
from numpy import add as ADD
from numpy import subtract as SUBTRACT
from numpy import arange as ARANGE
from numpy import all as ALL
from numpy import delete as DELETE
from numpy import exp as EXP
from numpy.random import rand as RAND
from numpy import absolute as ABS
from PIL import Image
import pathlib
import itertools
from numpy import unique as UNIQUE
from numpy import delete as DELETE
from numpy import all as ALL


class Helpers:

    @staticmethod
    def display(param_one, param_two, param_three):
        print("{0}'s {1} : {2}".format(param_one, param_two, param_three))

    @staticmethod
    def mixer_estimator(cluster_set, p_size):
        return [len(cluster_set[cluster])/p_size for cluster in cluster_set]

    @staticmethod
    def compute_neighbor(pixels, width, height, canny_edge):
        neighbor = []
        counter = 0
        for j, pixel in enumerate(pixels):
            if counter == width - 1:
                counter = 0
                neighbor.append({'index': None, 'pixel': None})
            else:
                neighbor.append({'index': j, 'pixel': pixels[j + 1]})
                counter += 1
        result = []
        boundary_pixels = []
        img_pixels = []
        n_pixels = ASARRAY(neighbor).reshape(width * height, 1)

        for i, (p_v, n_v, e_v) in enumerate(zip(pixels, n_pixels, canny_edge)):
            n_v = n_v[0]
            if e_v == 255 or n_v['index'] and canny_edge[n_v['index']] == 255:
                boundary_pixels.append({'index': i, 'pixel': p_v})
            else:
                result.append(n_v['pixel'])
                img_pixels.append(p_v)
        return ASARRAY(img_pixels), ASARRAY(result), ASARRAY(boundary_pixels), len(result)

    @staticmethod
    def split_pixels_based_on_label(labels, pixels):
        clusters_obj = {}
        for index, label in enumerate(labels):
            if not (label in clusters_obj):
                clusters_obj[label] = []
            clusters_obj[label].append(pixels[index])
        return clusters_obj, len(clusters_obj[0][0])

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def initial_mix(no_of_clusters):
        return ASARRAY(FULL((1, no_of_clusters), 1/no_of_clusters))

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """

    @staticmethod
    def method_of_moment(no_of_clusters, cluster_set, dimension):
        alpha = ZEROS((no_of_clusters, dimension + 1))
        for label in cluster_set:
            sum_alpha = 0
            for d in range(dimension):
                cluster_set[label] = ASARRAY(cluster_set[label])
                mean = MEAN(cluster_set[label][:, [d]])
                den = VAR(cluster_set[label][:, [d]]) + sys.float_info.epsilon
                alpha_d_plus_one = ((POW(mean, 2) + mean)/den) + 2
                sum_alpha += alpha_d_plus_one
                alpha[label][d] = mean * (alpha_d_plus_one - 1)
            alpha[label][dimension] = MEAN(sum_alpha)
        return NORMALIZE(alpha)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def posterior_estimator(pdf, mix, p_size, k):
        return ASARRAY([(POW(mix, 2) * p_v) / SUM(POW(mix, 2) * p_v) for p_v in pdf]).reshape(p_size, k)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def mix_estimator(posterior):
        return ASARRAY(SUM(posterior, axis=0)/len(posterior))

    def gradient_estimation(self, pixels, n_pixels, pixel_size, alpha, posterior, dimension):
        gradient_arr = []
        pixels = CONCAT((pixels, FULL((pixel_size, 1), 1)), axis=1)
        pixel_log = ASARRAY([LOG(pixel / (1 + SUM(pixel[:dimension]))) for pixel in pixels])
        n_p = []
        foo = []
        for n_v in n_pixels:

            if n_v is not None:
                n_p.append(LOG(n_v / (1 + SUM(n_v))))
                foo.append(LOG(1 / (1 + SUM(n_v))))
            else:
                n_p.append(FULL((1, dimension), 0)[0])
                foo.append(0)
        foo = ASARRAY(foo).reshape(pixel_size, 1)
        n_p_log = CONCAT((ASARRAY(n_p), foo), axis=1)

        for index, a_v in enumerate(alpha):
            gradient_arr.append(self.gradient_estimator(a_v, posterior[:, [index]], pixel_log, n_p_log, dimension))
        return ASARRAY(gradient_arr)

    @staticmethod
    def gradient_estimator(alpha, posterior, log_pixels, log_n_pixels, dimension):
        return SUM(posterior * (2 * (POLYGAMMA(0, SUM(alpha))
                                - POLYGAMMA(0, alpha))
                                + log_pixels + log_n_pixels), axis=0).reshape(dimension + 1, 1)

    @staticmethod
    def hessian(dim, posterior, alpha):
        diagonal = []
        constant = []
        h_a = []
        a_t = []
        for index, a_v in enumerate(alpha):
            p_sum = SUM(posterior[:, index]) + sys.float_info.epsilon
            a_t_gamma = POLYGAMMA(1, a_v)
            diagonal.append(2 * DIAGONAL(-1 * EXP(- LOG(p_sum) - LOG(a_t_gamma))).reshape(dim + 1, dim + 1))
            a_t_gamma_sum = POLYGAMMA(1, SUM(a_v))
            constant.append((a_t_gamma_sum * SUM(1 / a_t_gamma) - 1) * 2 * a_t_gamma_sum * p_sum)
            a = ((-1 / (2 * p_sum)) * 1/a_t_gamma).reshape(1, dim + 1)
            h_a.append(a)
            a_t.append(TRANSPOSE(a))

        return ASARRAY(diagonal), constant, ASARRAY(h_a), ASARRAY(a_t)

    @staticmethod
    def hessian_inverse(no_of_clusters, hessian_diagonal, hessian_constant, hessian_a, hessian_a_trans):
        return ASARRAY([hessian_diagonal[j] + hessian_constant[j]
                        * MATMUL(hessian_a_trans[j], hessian_a[j]) for j in range(no_of_clusters)])

    @staticmethod
    def alpha_updater(alpha_set, hessian_inv, gradient, no_of_clusters, dimension):
        return ASARRAY([SUBTRACT(alpha_set[j].reshape(dimension + 1, 1), 0.5 * MATMUL(hessian_inv[j], gradient[j]))
                        for j in range(no_of_clusters)]).reshape(no_of_clusters, dimension + 1)


    @staticmethod
    def predict_labels(posterior):
        return ASARRAY(posterior.argmax(axis=1))

    def cal_post_boundary_pixels(self, inv_dirichlet, b_pixels_with_index,
                                 posterior,  no_of_clusters, alpha, dim, mix):
        n_pixels = []  # as we do not have any neighbors for boundary pixels.
        b_pixels = [b_v['pixel'] for b_v in b_pixels_with_index]
        pdf_b_pixels = inv_dirichlet(no_of_clusters, alpha, b_pixels, n_pixels, dim).pdf_fetcher()
        posterior_b_pixels = self.posterior_estimator(pdf_b_pixels, mix, len(pdf_b_pixels), no_of_clusters)
        posterior = posterior.tolist()

        for i, p_v in enumerate(posterior_b_pixels):
            posterior.insert(b_pixels_with_index[i]['index'], p_v)
        return ASARRAY(posterior)

    @staticmethod
    def generate_img(no_of_clusters, predict_labels, img_width,  img_height
                     , img_name, img_extension, counter, p_color):
        # p_color = RAND(no_of_clusters, 3) * 0.45
        pixels = ND_ARRAY(shape=(img_width * img_height, 5), dtype=float)
        for cluster in range(no_of_clusters):
            for labelIndex, labelRecord in enumerate(predict_labels):
                if cluster == labelRecord:
                    pixels[labelIndex][0] = int(round(p_color[cluster][0] * 255))
                    pixels[labelIndex][1] = int(round(p_color[cluster][1] * 255))
                    pixels[labelIndex][2] = int(round(p_color[cluster][2] * 255))

        # Save image
        image = Image.new("RGB", (img_width, img_height))

        for y in range(img_height):
            for x in range(img_width):
                image.putpixel((x, y), (int(pixels[y * img_width + x][0]),
                                        int(pixels[y * img_width + x][1]),
                                        int(pixels[y * img_width + x][2])))
        pathlib.Path('./output/output_' + img_name).mkdir(parents=True, exist_ok=True)
        print("./output/output_" + img_name + "/" + str(counter) + img_extension)
        image.save("./output/output_" + img_name + "/" + str(counter) + img_extension)

    @staticmethod
    def save_labels(img_name, data, counter):
        np.save('./output/output_' + img_name + '/' + 'segment_obj_' + str(counter) + img_name, data)
