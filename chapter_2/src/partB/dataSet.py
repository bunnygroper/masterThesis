"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*/

"""


from PIL import Image
from numpy import ndarray as ND_ARRAY
from numpy import sum as SUM
import sys


class DataSet:

    def __init__(self, img_name):
        self.img_name = img_name

    """
        /**
         * `loadCsv` function of dataSet Class.
         * @return  {Integer Vector} vector X & Y.
        */
    """
    def pixel_extractor(self):
        # image = Image.open("./dataset/images/" + self.img_name)
        image = Image.open('/home/bugsbunny/Projects/masterThesis/BSDS500/data/images/test/' + self.img_name)
        img_width = image.size[0]
        img_height = image.size[1]
        # img_width = 3
        # img_height = 3

        # Initialise data vector with attribute r,g,b,x,y for each pixel
        pixels_vector = ND_ARRAY(shape=(img_width * img_height, 5), dtype=float)


        """
            /**
             * Populate data vector with data from input image dataVector has 5 fields: red, green, blue, x coord, y coord
             * @return  
            */
        """
        for y in range(0, img_height):
            for x in range(0, img_width):
                xy = (x, y)
                rgb = image.getpixel(xy)
                rgb_sum = SUM(rgb)
                if rgb_sum == 0:
                    rgb = [0, 0, 0]
                else:
                    rgb = (rgb / rgb_sum) * 255.0
                pixels_vector[x + y * img_width, 0] = rgb[0]
                pixels_vector[x + y * img_width, 1] = rgb[1]
                pixels_vector[x + y * img_width, 2] = rgb[2]
                pixels_vector[x + y * img_width, 3] = x
                pixels_vector[x + y * img_width, 4] = y

        return pixels_vector[:, :3], img_width, img_height

