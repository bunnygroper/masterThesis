import sys
from dataSet import DataSet  # Importing DataSet
from KMeans import KMeans as KM # contains KNN related functionality.
from lib.helpers import helpers as HELPER  # class contains the logic like performanceMeasure, Precision etc.
from lib.constants import CONST  # contains the constant values.
from gen_inverted_dirichlet import GeneralizedInvertedDirichlet as GID
from sklearn.preprocessing import normalize as NORMALIZE
from numpy import asarray as ASARRAY
import warnings
warnings.filterwarnings('error')



"""
/**
 * This function contains the logic for `Initial Algorithm`.
 * @param  {Integer} K: Number of Clusters.
 * @param {Float Array} which image pixels in array form having dimension  of (N * DIM).
 * @return {Float Array} alphaSet in array form having dimension  of (N * DIM).
 * @return {Float Array} imgPixels in array form having dimension  of (N * DIM).
 * @return {Integer} DIM: Number of Features 
 * @return {Integer} pixelSize: Size of img Array.
 * @return {Float Array} mix: Mixture Components value in (1 x N) dimention.
 * @return {Integer} imageW: Contains the width of an image.
 * @return {Integer} imageH: Contains the height of an image.
 */
"""


def initial_algorithm(k_param, img_name_param):
    pixels, image_width, image_height = DataSet(img_name_param).pixel_extractor()
    pixels = NORMALIZE(pixels)
    pixels += sys.float_info.epsilon
    p_size = len(pixels)
    p_labels = ASARRAY(KM(pixels, k_param).predict()).reshape(1, p_size)
    cluster_set, dim_initial = HELPER().split_pixels_based_on_label(p_labels[0], pixels)
    pixels = HELPER().geo_transformation(pixels, dim_initial, p_size)
    mix_initial_alg = HELPER().mixer_estimator(cluster_set, p_size, k_param)
    alpha_initial, beta_initial = HELPER().method_of_moment(k_param, cluster_set, dim_initial)
    return alpha_initial, beta_initial, pixels, dim_initial, p_size, mix_initial_alg, image_width, image_height


"""
/**
 * This function Contains the Estimation Step logic.
 * @param  {Integer} K.
 * @param  {Integer} mix.
 * @param  {Integer} alphaSet.
 * @param  {Integer} imgPixels.
 * @param  {Integer} pixelSize.
 * @return {String} pdfMatrix.
 * @return {String} posteriorProbability.
 */
"""


def estimation_step(k_param, mix_param, alpha_param, beta_param,  pixels_param, p_size, img_h, img_w):
    pdf_e_step = GID(k_param, alpha_param, beta_param, pixels_param).pdf_fetcher()
    posterior_e_step = HELPER().posterior_estimator(pdf_e_step, mix_param, p_size, k_param)
    mrf = HELPER().markov_random_fld(posterior_e_step, img_h, img_w, k_param, mix_param)
    return posterior_e_step, mrf


"""
/**
 * This function Contains the Maximization Step logic.
 * @param  {Integer} K.
 * @param  {Integer} alphaSet.
 * @param  {Integer} imgPixels.
 * @param  {Integer} DIM.
 * @param  {Integer} posteriorProb.
 * @param  {Integer} pixelSize.
 * @param  {Integer} imageH.
 * @param  {Integer} imageW.
 * @param  {Integer} mix.
 * @return {String} mix.
 * @return {String} alpha.
 * @return {String} mRandomField.
 */
"""


def maximization_step(k_param, alpha_param, beta_param, pixels_param, dim_param, posterior_param, mrf_param):
    mix_m_step = HELPER().mix_updater(posterior, mrf_param)
    q_alpha, q_beta, q_alpha_sqr, q_beta_sqr, q_alpha_beta_sqr = HELPER().compute_g(pixels_param, alpha_param,
                                                                                    beta_param, posterior_param,
                                                                                    dim_param, k_param)
    alpha_m_step, beta_m_step = HELPER().fisher_info_inverse(alpha, beta, q_alpha, q_beta, q_alpha_sqr,
                                                             q_beta_sqr, q_alpha_beta_sqr, k_param, dim_param)
    return mix_m_step, alpha_m_step, beta_m_step



"""
/**
 * This function add the array's element and return them in the form of a String.
 * @param  {Integer} a.
 * @return {String} which contains the Sum of Array.
 */
"""

if __name__ == '__main__':
    k = CONST["K"]

    if len(sys.argv) == 2:
        img = sys.argv[1]
    else:
        img = CONST['IMG']
    img_arr = img.split(".")

    if len(img_arr) == 2:
        img_name = img_arr[0]
        img_extension = '.' + img_arr[1]
    else:
        img_name = img_arr[0]
        img_extension = '.jpg'

    alpha, beta, img_pixels, dimension, pixel_size, mix, img_width, img_height = initial_algorithm(k, img)
    counter = 1

    while True:
        posterior, m_random_fld = estimation_step(k, mix, alpha, beta, img_pixels,
                                                  pixel_size, img_height, img_width)
        mix, alpha, beta = maximization_step(k, alpha, beta, img_pixels, dimension,
                                             posterior, m_random_fld)
        predict_labels = HELPER().predict_labels(posterior)
        print("Counter :>", counter)
        HELPER().generate_img(k, dimension, predict_labels, img_width, img_height, img_pixels,
                              img_name, img_extension, counter, CONST["PIXEL_COLOR_ONE"])
        HELPER().save_labels(img_name, predict_labels, counter)
        counter = counter + 1
        if counter == 3:
            exit()


