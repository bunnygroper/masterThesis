"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*/

"""

CONST = {}
CONST["K"] = 10
CONST["IMG"] = "1.jpg"
CONST["cluster_drop_cond"] = 0.0001
CONST["algConverge"] = 0.01
CONST["PIXEL_COLOR"] = [[0.40111238, 0.52091329, 0.11624053],
                        [0.50359309, 0.25279871, 0.30752668],
                        [0.66238654, 0.16715697, 0.33143084]]

CONST["PIXEL_COLOR_ONE"] = [[0.07939081, 0.47110583, 0.23314231],
                            [0.03459316,  0.116686, 0.26123752],
                            [0.43340025,  0.60235897,  0.13350015]]