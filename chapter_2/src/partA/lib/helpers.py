"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*/

"""


import sys
import numpy as np
import pickle
from math import pow as POW
from scipy.special import gamma as GAMMA
from scipy.special import polygamma as POLYGAMMA
from numpy import ndarray as ND_ARRAY
from numpy import sum as SUM
from numpy import multiply as MUL
from numpy import log as LOG
from numpy import zeros as ZEROS
from numpy import transpose as TRANSPOSE
from numpy import asarray as ASARRAY
from numpy import mean as MEAN, var as VAR
from numpy import full as FULL
from numpy import concatenate as CONCAT
from sklearn.preprocessing import normalize as NORMALIZE
from numpy.linalg import inv as INVERSE
from numpy import diag as DIAGONAL
from numpy import matmul as MATMUL
from numpy import add as ADD
from numpy import subtract as SUBTRACT
from numpy import arange as ARANGE
from numpy import all as ALL
from numpy import delete as DELETE
from numpy import exp as EXP
from numpy.random import rand as RAND
from numpy import absolute as ABS
from PIL import Image
import pathlib
import itertools
from numpy import unique as UNIQUE
from numpy import delete as DELETE
from numpy import all as ALL


class Helpers:


    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def display(param_one, param_two, param_three):
        print("{0}'s {1} : {2}".format(param_one, param_two, param_three))

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def mixer_estimator(cluster_set, p_size):
        return [len(cluster_set[cluster])/p_size for cluster in cluster_set]

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def split_pixels_based_on_label(labels, pixels):
        clusters_obj = {}
        for index, label in enumerate(labels):
            if not (label in clusters_obj):
                clusters_obj[label] = []
            clusters_obj[label].append(pixels[index])
        return clusters_obj, len(clusters_obj[0][0])

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def initial_mixer_estimator(mix, p_size, k):
        return ASARRAY([FULL((p_size, 1), pi) for pi in mix]).T.reshape(p_size, k)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """

    @staticmethod
    def method_of_moment(no_of_clusters, cluster_set, dimension):
        alpha = ZEROS((no_of_clusters, dimension + 1))
        for label in cluster_set:
            sum_alpha = 0
            for d in range(dimension):
                cluster_set[label] = ASARRAY(cluster_set[label])
                mean = MEAN(cluster_set[label][:, [d]])
                den = VAR(cluster_set[label][:, [d]]) + sys.float_info.epsilon
                alpha_d_plus_one = ((POW(mean, 2) + mean)/den) + 2
                sum_alpha += alpha_d_plus_one
                alpha[label][d] = mean * (alpha_d_plus_one - 1)
            alpha[label][dimension] = MEAN(sum_alpha)
        return NORMALIZE(alpha)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def posterior_estimator(pdf, mix, p_size, k):
        return ASARRAY([(m * p_v)/SUM(m * p_v) for m, p_v in zip(mix, pdf)]).reshape(p_size, k)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def mix_estimation(posterior, mrf):
        return ASARRAY([(p_v + m_v)/SUM(p_v + m_v) for p_v, m_v in zip(posterior, mrf)])

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    def gradient_estimation(self, pixels, pixel_size, alpha, posterior, dimension):
        gradient_arr = []
        pixels = CONCAT((pixels, FULL((pixel_size, 1), 1)), axis=1)
        pixel_log = ASARRAY([LOG(pixel / (1 + SUM(pixel[:dimension]))) for pixel in pixels])
        for index, a_v in enumerate(alpha):
            gradient_arr.append(self.gMatrixGenerator(a_v, posterior[:, [index]], pixel_log, dimension))
        return ASARRAY(gradient_arr)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    def gMatrixGenerator(self, alpha, posterior, logPixels, DIM):
        return SUM(posterior * (POLYGAMMA(0, SUM(alpha)) - POLYGAMMA(0, alpha) + logPixels), axis=0).reshape(DIM + 1, 1)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def hessian(dim, posterior, alpha):
        diagonal = []
        constant = []
        h_a = []
        a_t = []
        for index, a_v in enumerate(alpha):
            p_sum = SUM(posterior[:, index]) + sys.float_info.epsilon
            a_t_gamma = POLYGAMMA(1, a_v)
            diagonal.append(DIAGONAL(-1 * EXP(- LOG(p_sum) - LOG(a_t_gamma))).reshape(dim + 1, dim + 1))
            a_t_gamma_sum = POLYGAMMA(1, SUM(a_v))
            constant.append((a_t_gamma_sum * SUM(1 / a_t_gamma) - 1) * a_t_gamma_sum * p_sum)
            a = ((-1 / p_sum) * 1/a_t_gamma).reshape(1, dim + 1)
            h_a.append(a)
            a_t.append(TRANSPOSE(a))

        return ASARRAY(diagonal), constant, ASARRAY(h_a), ASARRAY(a_t)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    def hessian_inverse(self, K, hessian_Diagonal, hessian_constant, hessian_a, hessian_a_T):
        return ASARRAY([hessian_Diagonal[j] + hessian_constant[j]
                        * MATMUL(hessian_a_T[j], hessian_a[j]) for j in range(K)])

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    def alpha_updator(self, alphaSet, hessian_Inverse, G, K, DIM):
        # return ASARRAY([SUBTRACT(alphaSet[j].reshape(DIM + 1, 1), MATMUL(hessian_Inverse[j], G[j]) * 1.5)
        #                 for j in range(K)]).reshape(K, DIM + 1)
        return ASARRAY([SUBTRACT(alphaSet[j].reshape(DIM + 1, 1), MATMUL(hessian_Inverse[j], G[j]))
                        for j in range(K)]).reshape(K, DIM + 1)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """

    def markov_random_fld(self, posterior, img_height, img_width, k, mix):
        return ASARRAY([self.window(posterior[:, j: j + 1].reshape(img_height, img_width),
                                    img_height, img_width, mix[:, j:j + 1].reshape(img_height, img_width))
                        for j in range(k)]).T.reshape(img_height * img_width, k)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def window(posterior, img_height, img_width, mix):
        a = 0
        b = 5
        result = []
        for h in range(img_height):
            c = 0
            d = 5
            for w in range(img_width):
                result.append(EXP((12/(2 * posterior[a:b, c:d].size)) * SUM(posterior[a:b, c:d] + mix[a:b, c:d])))
                c = c + 1
                d = d + 1
            a = a + 1
            b = b + 1
        return ASARRAY(result).reshape(img_height * img_width, 1)

    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    def clusterDropTest(self, mix, alpha, dropingCriteria, K, DIM, pixelSize):
        # print("Inside a ClusterDropTest()!")
        mixInfo = []
        alphaInfo = []
        # print("\n######### Inside Cluster Drop Test ##########\n")
        for j in range(K):
            if SUM(mix[:, j: j+1]) > dropingCriteria:
                mixInfo.append(mix[:, j: j+1])
                alphaInfo.append(alpha[j])
            else:
                print("Cluster having  alpha :", alpha[j], " & Mix :", j, " is removed!")
        return ASARRAY(mixInfo).T.reshape(pixelSize, len(alphaInfo)), \
            ASARRAY(alphaInfo).reshape(len(alphaInfo), DIM + 1), len(mixInfo)

    @staticmethod
    def log_likelihood(posterior, pdf, mix, m_r_field):
        return SUM(posterior * (LOG(mix) + LOG(pdf)) + m_r_field * LOG(mix))



    """
    /**
     * This function add the array's element and return them in the form of a String.
     * @param  {Integer} a.
     * @return {String} which contains the Sum of Array.
     */
    """
    @staticmethod
    def predict_labels(posterior):
        return ASARRAY(posterior.argmax(axis=1))

    @staticmethod
    def generate_img(pixels, no_of_clusters, predict_labels, img_width,  img_height
                     , img_name, img_extension, counter, p_color):
        p_color = RAND(no_of_clusters, 3) * 0.45
        # pixels = ND_ARRAY(shape=(img_width * img_height, 5), dtype=float)
        for cluster in range(no_of_clusters):
            for labelIndex, labelRecord in enumerate(predict_labels):
                if cluster == labelRecord:
                    pixels[labelIndex][0] = int(round(p_color[cluster][0] * 255))
                    pixels[labelIndex][1] = int(round(p_color[cluster][1] * 255))
                    pixels[labelIndex][2] = int(round(p_color[cluster][2] * 255))

        # Save image
        image = Image.new("RGB", (img_width, img_height))

        for y in range(img_height):
            for x in range(img_width):
                image.putpixel((x, y), (int(pixels[y * img_width + x][0]),
                                        int(pixels[y * img_width + x][1]),
                                        int(pixels[y * img_width + x][2])))
        pathlib.Path('./output/output_' + img_name).mkdir(parents=True, exist_ok=True)
        print("./output/output_" + img_name + "/" + str(counter) + img_extension)
        image.save("./output/output_" + img_name + "/" + str(counter) + img_extension)

    @staticmethod
    def save_labels(img_name, data, counter):
        np.save('./output/output_' + img_name + '/' + 'segment_obj_' + str(counter) + img_name, data)

    def cluster_kicker(self, mix, alpha):
        mix, a = self.eliminate_col_by_index(mix, alpha)
        return mix, a, len(mix.T)

    @staticmethod
    def eliminate_col_by_index(foo, alpha):
        soo= []
        doo = []
        for i, j in enumerate(foo.T):

            if not np.all(j < 0.0001):
                soo.append(alpha[i])
                doo.append(j)
        return ASARRAY(doo).T, ASARRAY(soo)
