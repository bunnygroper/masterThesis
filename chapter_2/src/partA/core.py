"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*
*   EM Algorithm Inverted Dirichlet Mixture Model.
*       1) Convert image pixels into array.
*       2) Normalize it.
*       3) Assume Number of Cluster(K) =  10 & apply KMeans clustering algorithm
*          to obtain K clusters for Initialization purposes.
*       4) Use `Method of Moments` for obtaining the initial values for Mixing Parameters.
*       5) Expectation Step:
*                           => Compute the Posterior Probability.
*       6) Maximization Step:
*                           => Update the Mixing Parameter.
*                           => Update the Alpha Parameter using `Newton Raphson` Method.
*       7) If Mixing Parameter of any Cluster < Cluster-Skipping Threshold:
*                           => Skip that particular Cluster.
*       8) Compute the Log Likelihood and check for Convergence by comparing the difference
*          between last two likelihood values with the Convergence Threshold.
*       9) If algorithm(converge) :terminate
*       10) Go-to Step 5(Expectation Step).
*/

"""
import sys
from dataSet import DataSet  # Importing DataSet
from KMeans import KMeans as KM # contains KNN related functionality.
from gaussian import Gaussian as GMM
from lib.helpers import Helpers as HELPER  # class contains the logic like performanceMeasure, Precision etc.
from lib.constants import CONST  # contains the constant values.
from invertedDirichlet import InvertedDirichlet
from sklearn.preprocessing import normalize as NORMALIZE
from numpy import sum as SUM
from numpy import asarray as ASARRAY


"""
/**
 * This function contains the logic for `Initial Algorithm`.
 * @param  {Integer} K: Number of Clusters.
 * @param {Float Array} which image pixels in array form having dimension  of (N * DIM).
 * @return {Float Array} alphaSet in array form having dimension  of (N * DIM).
 * @return {Float Array} imgPixels in array form having dimension  of (N * DIM).
 * @return {Integer} DIM: Number of Features 
 * @return {Integer} pixelSize: Size of img Array.
 * @return {Float Array} mix: Mixture Components value in (1 x N) dimention.
 * @return {Integer} imageW: Contains the width of an image.
 * @return {Integer} imageH: Contains the height of an image.
 */
"""


def initial_algorithm(k, img_name):
    pixels, image_width, image_height = DataSet(img_name).pixel_extractor()
    pixels = NORMALIZE(pixels)
    pixels += sys.float_info.epsilon
    p_size = len(pixels)
    p_labels = ASARRAY(KM(pixels, K).predict()).reshape(1, p_size)
    cluster_set, dim = HELPER().split_pixels_based_on_label(p_labels[0], pixels)
    pi_k_means = HELPER().mixer_estimator(cluster_set, p_size)
    alpha_initial_alg = HELPER().method_of_moment(K, cluster_set, dim)
    mix_initial_alg = HELPER().initial_mixer_estimator(pi_k_means, p_size, k)
    return alpha_initial_alg, pixels, dim, p_size, mix_initial_alg, image_width, image_height


"""
/**
 * This function Contains the Estimation Step logic.
 * @param  {Integer} K.
 * @param  {Integer} mix.
 * @param  {Integer} alphaSet.
 * @param  {Integer} imgPixels.
 * @param  {Integer} pixelSize.
 * @return {String} pdfMatrix.
 * @return {String} posteriorProbability.
 */
"""


def estimation_step(k, mix_param, alpha_param, pixels, p_size, image_width, image_height):
    pdf_e_step = InvertedDirichlet(k, alpha_param, pixels).pdf_fetcher()
    posterior_e_step = HELPER().posterior_estimator(pdf_e_step, mix_param, p_size, k)
    mrf_e_step = HELPER().markov_random_fld(posterior_e_step, image_height, image_width, k, mix_param)
    return pdf_e_step, posterior_e_step, mrf_e_step


"""
/**
 * This function Contains the Maximization Step logic.
 * @param  {Integer} K.
 * @param  {Integer} alphaSet.
 * @param  {Integer} imgPixels.
 * @param  {Integer} DIM.
 * @param  {Integer} posteriorProb.
 * @param  {Integer} pixelSize.
 * @param  {Integer} imageH.
 * @param  {Integer} imageW.
 * @param  {Integer} mix.
 * @return {String} mix.
 * @return {String} alpha.
 */
"""


def maximization_step(no_of_clusters, alpha_param, pixels, dim, posterior_param, p_size, mrf):
    mix_m_step = HELPER().mix_estimation(posterior_param, mrf)  # Checked: Working Fine!
    gradient = HELPER().gradient_estimation(pixels, p_size, alpha_param, posterior_param, dim)  # Checked: Working Fine!
    h_diagonal, h_constant, hessian_a, hessian_a_t = HELPER().hessian(dim, posterior_param, alpha_param)
    hessian_inverse = HELPER().hessian_inverse(no_of_clusters, h_diagonal, h_constant, hessian_a, hessian_a_t)
    alpha_m_step = HELPER().alpha_updator(alpha_param, hessian_inverse, gradient, no_of_clusters, dim)
    return mix_m_step, alpha_m_step


"""
/**
 * This function add the array's element and return them in the form of a String.
 * @param  {Integer} a.
 * @return {String} which contains the Sum of Array.
 */
"""

if __name__ == '__main__':
    K = CONST["K"]

    if len(sys.argv) == 2:
        img = sys.argv[1]
    else:
        img = CONST['IMG']
    imgArr = img.split(".")

    if len(imgArr) == 2:
        image_name = imgArr[0]
        img_extension = '.' + imgArr[1]
    else:
        image_name = imgArr[0]
        img_extension = '.jpg'

    alpha, img_pixels, dimension, pixel_size, mix, img_width, img_height = initial_algorithm(K, img)
    counter = 1
    while True:
        pdf, posterior, markov_random_fld = estimation_step(K, mix, alpha, img_pixels, pixel_size, img_width, img_height)
        mix, alpha = maximization_step(K, alpha, img_pixels, dimension, posterior, pixel_size, markov_random_fld)
        predict_labels = HELPER().predict_labels(posterior)
        HELPER().generate_img(img_pixels, K, predict_labels, img_width, img_height, image_name,
                              img_extension, counter, CONST["PIXEL_COLOR_ONE"])
        # mix, alpha, K = HELPER().cluster_kicker(mix, alpha)
        print("Alpha :>", alpha)
        HELPER.save_labels(image_name, predict_labels, counter)
        counter = counter + 1
        if counter == 3:
            exit()
