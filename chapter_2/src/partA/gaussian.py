"""
/*
*       Coded by : Jaspreet Singh Kalsi.
*
*       "Thesis  Chapter-2 Part A
*       (Image Fragmentation using Inverted Dirichlet Distribution using Markov Random Field as a Prior).
*
*       ```python core.py <Image-Name>```
*
*/

"""
from sklearn.mixture import GaussianMixture as GMM


class Gaussian:

    """
        /**
         * Constructor of KNN Class.
         * @param  {Integer Vector} X_train(Training Data).
         * @param  {Integer Vector} Y_train(Training Class Label).
         * @param  {Integer Vector} X_test(Test Data).
         * @param  {Integer Vector} Y_test(Test Class Label).
        */
     """

    def __init__(self, pixels, k):
        self.pixels = pixels
        self.GMM = GMM(n_components=k).fit(self.pixels)

    """
        /**
         * `predict` function of KNN Class.
         * @return  {Integer Vector} Y_Predict(Predicted Class Label for Test Data).
        */
     """
    def predict(self):
        return self.GMM.predict(self.pixels)
